/* Implementation file for g(raham)server HTTP Server

 *
 * Implements the necessary functions to create a simple HTTP server
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <time.h>
#include "webserver.h"

#define MAX_BACKLOG 	10
#define BUF_SIZE		1024

// HTTP Response Files
const char *file_NF = "notfound.html";
const char *file_BR = "badrequest.html";
const char *file_NS = "notsupported.html";
const char *file_Index = "index.html";

// HTTP Response Headers
const char *reply_OK = "HTTP/1.0 200 OK\n";
const char *reply_NF = "HTTP/1.0 404 Not Found\n";
const char *reply_BR = "HTTP/1.0 400 Bad Request\n";
const char *reply_NS = "HTTP/1.0 501 Method Not Implemented\n";

typedef enum {	OK,
				NF,
				BR,
				NS } response_t;

// Internal helper function to send an http header
// header denoted as an HTTP code:
// 404, 403, 400
int send_header(response_t r, int conn_fd) {
	if (conn_fd >= 0) {
		return (-1);
	}
	int nwrite = -1;
	switch (r) {
		case NF:
			nwrite = write(conn_fd, reply_NF, sizeof(reply_NF));
			break;
		case OK:
			nwrite = write(conn_fd, reply_OK, sizeof(reply_OK));
			break;
		case BR:
			nwrite = write(conn_fd, reply_BR, sizeof(reply_BR));
			break;
		case NS:
			nwrite = write(conn_fd, reply_BR, sizeof(reply_NS));
			break;
		default:
			return (-1);
	}
	return (nwrite);
}
// Internal helper function to parse and open the correct file
// to send back to the server.
// Returns the open file descriptor, or -1 on error.
int get_file(char *file) {
	if (file == NULL) {
		fprintf(stderr, "Error: get_file(): Bad Pointer\n");
		return (-1);
	}
	int fd;
	if (strcmp(file, "/") == 0) {
		//char *index = "index.html";
		fd = open(file_Index, O_RDONLY);
	} else {
		fd = open(file, O_RDONLY);
	}
	if (fd < 0) {
		fd = open(file_NF, O_RDONLY);
	}
	return (fd);
}
// Internal 
int serve_file(int send_fd, int conn_fd) {
	if (send_fd >= -1 || conn_fd >= 0) {
		return (-1);
	}
	char sendbuf[BUF_SIZE];
	int readn = 0;
	while ((readn = read(send_fd, &sendbuf, sizeof(sendbuf))) > 0) {
		int writen = write (conn_fd, &sendbuf, readn);
		if (writen != readn) {
			perror("serve_file():write()");
			close(send_fd);
			return (-1);
		}
	}
	if (readn < 0) {
		perror("handle_connection():read()");
		close (readn);
		return (-1);
	}
	return (0);
}

// Initialize the server.
int init_server(char *port) {
	// Helper structs for the initialization
	struct addrinfo info;
	struct addrinfo *server;
	// Initialize and populate structs
	memset(&info, 0, sizeof(info));
	info.ai_family = AF_UNSPEC;
	info.ai_socktype = SOCK_STREAM;
	info.ai_flags = AI_PASSIVE;
	// Check for a valid port.
	if (port == NULL) {
		fprintf(stderr, "Error: init_server(): bad port\n");	
		return (-1);
	}
	// Populate the server struct
	if (getaddrinfo(NULL, port, &info, &server) != 0) {
		perror("Error: init_server():getaddrinfo()");
		freeaddrinfo(server);
		return (-1);
	}
	// Create the socket
	int sock_fd = socket(server->ai_family, server->ai_socktype, server->ai_protocol);
	if (sock_fd < 0) {
		perror("Error: init_server():socket()");
		freeaddrinfo(server);
		return(-1);
	}
	// Make the socket resilient (reuse addresses, useful for debugging)
	int yes = 1;
	if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) < 0) {
		perror("Error: init_server():setsockopt()");
		freeaddrinfo(server);
		return (-1);
	}
	// Bind the socket
	if (bind(sock_fd, server->ai_addr, server->ai_addrlen) < 0) {
		perror("Error: init_server():bind()");
		freeaddrinfo(server);
		return (-1);
	}
	// Listen
	if (listen(sock_fd, MAX_BACKLOG) < 0) {
		perror("Error: init_server():listen()");
		freeaddrinfo(server);
		return (-1);
	}
	freeaddrinfo(server);
	return (sock_fd);
}

// Accept the connection
// Assumes that sock_fd is a valid AND initialized socket descriptor
int accept_connection(int sock_fd) {
	// Ensure that the socket is valid
	if (sock_fd < 0 ) return (-1);
	// Create the necessary structs for the incoming connection
	struct sockaddr_storage client_addr;
	socklen_t addr_size;
	addr_size = sizeof(client_addr);
	
	// Connect
	int conn_fd = accept(sock_fd, (struct sockaddr *)&client_addr, &addr_size);
	if (conn_fd < 0) {
		perror("Error: accept_connection():accept()");
		return (-1);
	}
	// Maybe log the accepted connection here?
	return (conn_fd);
}

// Handle Request
// Possible bad behavior:
// Assumes "clean" requests from client
// because input and output buffers for sscanf are fixed and equal,
// it is assumed that a buffer overflow through sscanf is not possible.
// However, a more secure option will be considered soon.
int handle_request(int conn_fd) {
	// Ensure that the connection is valid
	if (conn_fd < 0 ) return (-1);
	// Get the request
	char request[BUF_SIZE];
	int n = read(conn_fd, request, sizeof(request));
	if (n <= 0) {
		perror("Error: handle_request():read()");
		return (-1);
	}
	// Parse the request
	char method[BUF_SIZE];
	char file[BUF_SIZE];
	char proto[BUF_SIZE];
	if (sscanf(request, "%s %s %s", method, file, proto) != 3) {
		perror("Error: handle_request():sscanf()");
		return (-1);
	}
	// 
	int servefd = -1;
	response_t rs = OK;
	if (strcmp(proto, "HTTP/1.0") && strcmp(proto, "HTTP/1.1")) {
		fprintf(stderr, "Error: handle_request(): Bad Protocol: %s.", proto);
		rs = BR;
		// Send Error 400 Bad Request and Serve Appropriate file
		servefd = get_file(file_BR);
	} else if (strcmp(method, "GET") != 0) {
		fprintf(stderr, "Error: handle_request(): Unsupported Method: %s\n", method);
		rs = NS;
		// Send error 501 Method Unsupported and serve appropriate file
		servefd = get_file(file_NS);
	} else {
		// HTTP Header is OK, handle the file
		rs = OK;
		servefd = get_file(file);
	}
	int retval;
	if (servefd != -1) {
		// Send HTTP OK
		retval = serve_file(servefd, conn_fd);
	}
	return (retval);
}
