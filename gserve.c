#include <stdio.h>
#include <stdlib.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <wait.h>
#include <unistd.h>
#include "webserver.h"

int main(int argc, char **argv) {
	int sock_fd = init_server("8080");
	if (sock_fd < 0) {
		printf("init_server failed\n");
		exit (0);
	}
	while (1) {
		int conn_fd = accept_connection(sock_fd);
		if (conn_fd < 0) {
			printf("accept_connection failed\n");
			continue;
		}
		int pid = fork();
		if (pid < 0) {
			perror("Error: fork()");
			exit (1);
		} else if (pid == 0) {
			close(sock_fd);
			int rv = handle_request(conn_fd);
			close(conn_fd);
			exit(rv);
		} else {
			close(conn_fd);
			waitpid(pid, NULL, 0);
		}
	}
	close(sock_fd);
}
