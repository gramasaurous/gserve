# Graham Greving
# ggreving

MKFILE = Makefile
GMAKE = gmake --no-print-directory

GCC = gcc -g -O0 -Wall -Wextra -std=gnu99
MKDEPS = gcc -MM

CSOURCE = gserve.c webserver.c
CHEADER = webserver.h
OBJECTS = ${CSOURCE:.c=.o}
EXECBIN = gserve

all : ${EXECBIN}

${EXECBIN} : ${OBJECTS}
	${GCC} -o $@ ${OBJECTS}

%.o : %.c
	${GCC} -c $<

clean :
	- rm ${OBJECTS} 

spotless : clean
	- rm ${EXECBIN}

git:
	- git commit -a

again :
	${GMAKE} spotless deps all

