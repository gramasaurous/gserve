/*
 * Header File for the g(raham)serve HTTP Server
 * Author: Graham Greving
 * 
 * Outlines necessary functions to implement a rudimentary http server
 *
 */

#ifndef _GSERVE_H_INCLUDE_ 
#define _GSERVE_H_INCLUDE_

/* init_server
 * 
 * Creates a new instantiation of the http server on the given port
 * 
 * Returns a fully initialized socket listening on the requested port.
 * Returns -1 on error and prints the error to stderr.
 */
int init_server(char *port);

/* accept_connection
 *
 * Given an established socket, accepts a connection and returns the
 * new socket descriptor to be read and written to.
 * Returns -1 on error, and prints the error to stderr.
 */
int accept_connection(int sock_fd);

/* handle_request
 *
 * Given an accepted connection, interprets the incoming stream as an
 * HTTP request and handles it accordingly.
 *
 * Returns 0 on success and -1 on error, printing the error to stderr.
 */
int handle_request(int conn_fd);

/* log_request
 * 
 * Given an open connection logs the time, ip and port in a file with the
 * day's date as the name in the directory passed as 'path'.
 * Returns 0 on success and -1 on error, prints the error to stderr.
 */
int log_request(int conn_fd, char *path);

#endif